<?php
class Cat {
    public $warnaKucing;
    public $jenisKucing;
    public $warnaMata; 

    //constructor adalah sebuah function dimana 
    //dia akan dipanggil pertama setelah pembuatan objek 
    function __construct($warnaKucing, $jenisKucing, $warnaMata){
        $this->warnaKucing = $warnaKucing;
        $this->jenisKucing = $jenisKucing;
        $this->warnaMata = $warnaMata;
    }

    function mengeong(){
        echo 'Meeeooonggggg..........';
    }
}

$cat = new Cat("Pink", "Kucing Biasa", "Coklat");
$catAnggora = new Cat("Putih", "Anggora", "Biru");
$catKampung = new Cat("Kuning", "Kampung", "Hitam");

echo $cat->jenisKucing; //Persia 
echo 'Saya punya kucing '.$cat->warnaKucing.' dan jenisnya adalah '.$cat->jenisKucing.' dan punya mata '.$cat->warnaMata;
echo $cat->mengeong();

echo '<p>Saya punya kucing '.$catAnggora->warnaKucing.' dan jenisnya adalah '.$catAnggora->jenisKucing. '</p>';
echo '<p>Saya punya kucing '.$catKampung->warnaKucing.' dan jenisnya adalah '.$catKampung->jenisKucing;

?>  

<!-- Kucing BiasaSaya punya kucing Pink dan jenisnya adalah Kucing Biasa dan punya mata CoklatMeeeooonggggg..........
Saya punya kucing Putih dan jenisnya adalah Anggora

Saya punya kucing Kuning dan jenisnya adalah Kampung -->