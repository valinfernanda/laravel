<?php
// buatlah objek mobil dengan atribut jenis mobil dan warna mobil 
// memiliki method menyalakan mesin 
class Car { 
    public $jenisMobil = "";
    public $warnaMobil = "";

    function __construct($jenisMobil, $warnaMobil) { 
        $this->warnaMobil = $warnaMobil; 
        $this->jenisMobil = $jenisMobil;
    }

    function startEngine(){
        echo 'Brmmmmmm....';
    }
}

$carSedan = new Car("Sedan", "Black");
echo 'Saya punya mobil '.$carSedan->jenisMobil.' dengan warna ' .$carSedan->warnaMobil;


// buatlah handphone dengan atribut kualitas kamera, nama HP dan method untuk mengambil foto
class Handphone {
    public $kualitasKamera = '';
    public $namaHp = '';

    function __construct($kualitasKamera, $namaHp){
        $this->kualitasKamera = $kualitasKamera;
        $this->namaHp = $namaHp;
    }

    function takePhoto(){
        echo 'Ceklek';
    }
}

$hpSamsung = new Handphone("ausgezeichnet", "Samsung");
echo '<p>Saya punya HP '.$hpSamsung->namaHp.' dan kualitas kameranya '.$hpSamsung->kualitasKamera.'</p>';
$hpSamsung->takePhoto(); 
?>

