<?php 

//apabila kita pake constructor di parent class, maka harus pake di constructor di child classnya juga
class Handphone {
    public $warna;
    public $bahan;

    public function __construct($warna, $bahan){
        $this->warna = $warna;
        $this->bahan = $bahan;
    }

    //akan kita override 
    function phoneCall(){
        echo 'Klingeln.....';
    }

    //sdc
    function kirimPesan(){
        echo 'Kirim SMS....';
    }
}

class Smartphone extends Handphone {
    public $os; 

    public function __construct($warna, $bahan, $os){
         parent::__construct($warna, $bahan);
         $this->os=$os;
    }

    //ini overriding
    function phoneCall(){
        echo 'Teneng....';
    }

    //overloading
    function kirimPesan($namaOrang){
        echo 'Sms ke '.$namaOrang;
    }
}

$handphone = new Handphone("Kuning", "Iron");
$smartPhone = new Smartphone("Hijau", "Plastik", "ios");

// echo $smartPhone->warna;
// echo $smartPhone->os;

// echo $handphone->phoneCall(); //Klingeln
// echo $smartPhone->phoneCall(); //Teneng 

echo $handphone->kirimPesan(); //Klingeln
echo $smartPhone->kirimPesan("Yuhu"); //Teneng 


?>